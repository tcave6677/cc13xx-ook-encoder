/******************************************************************************

 @file rsl_336.c

 @brief API's for sending RSL336 encoded RF packets

 Group: WCS LPC
 $Target Device: DEVICES $

 ******************************************************************************
 $License: BSD3 2016 $
 ******************************************************************************
 $Release Name: PACKAGE NAME $
 $Release Date: PACKAGE RELEASE DATE $
 *****************************************************************************/

/***** Includes *****/
#include "rsl_336/rsl_336.h"
#include "ook_line_coding/ook_line_coding.h"
#include "smartrf_settings/smartrf_settings.h"

/***** Defines *****/
#define RSL336_FREQUENCY    43392000
#define RSL336_BIT_RATE     7999

#define RSL336_NUM_REPEAT_COMMANDS  6

#define RSL_GROUP_ADDR_0    OOKLC_SYMBOL_0 OOKLC_SYMBOL_F OOKLC_SYMBOL_F OOKLC_SYMBOL_F
#define RSL_GROUP_ADDR_1    OOKLC_SYMBOL_F OOKLC_SYMBOL_0 OOKLC_SYMBOL_F OOKLC_SYMBOL_F
#define RSL_GROUP_ADDR_2    OOKLC_SYMBOL_F OOKLC_SYMBOL_F OOKLC_SYMBOL_0 OOKLC_SYMBOL_F
#define RSL_GROUP_ADDR_3    OOKLC_SYMBOL_F OOKLC_SYMBOL_F OOKLC_SYMBOL_F OOKLC_SYMBOL_0

#define RSL_ADDR_0      OOKLC_SYMBOL_0 OOKLC_SYMBOL_F OOKLC_SYMBOL_F OOKLC_SYMBOL_F
#define RSL_ADDR_1      OOKLC_SYMBOL_F OOKLC_SYMBOL_0 OOKLC_SYMBOL_F OOKLC_SYMBOL_F
#define RSL_ADDR_2      OOKLC_SYMBOL_F OOKLC_SYMBOL_F OOKLC_SYMBOL_0 OOKLC_SYMBOL_F
#define RSL_ADDR_3      OOKLC_SYMBOL_F OOKLC_SYMBOL_F OOKLC_SYMBOL_F OOKLC_SYMBOL_0

#define RSL_ON OOKLC_SYMBOL_F   OOKLC_SYMBOL_F
#define RSL_OFF OOKLC_SYMBOL_F  OOKLC_SYMBOL_0

#define RSL_GROUP_ADDR_OFFSET   0
#define RSL_GROUP_ADDR_SIZE     (4 * OOKLC_BYTES_PER_SYMBOL)

#define RSL_ADDR_OFFSET         (RSL_GROUP_ADDR_OFFSET + RSL_GROUP_ADDR_SIZE)
#define RSL_ADDR_SIZE           (4 * OOKLC_BYTES_PER_SYMBOL)

#define RSL_RSVD_OFFSET         (RSL_ADDR_OFFSET + RSL_ADDR_SIZE)
#define RSL_RSVD_SIZE           (2 * OOKLC_BYTES_PER_SYMBOL)

#define RSL_STATE_OFFSET        (RSL_RSVD_OFFSET + RSL_RSVD_SIZE)
#define RSL_STATE_SIZE          (2 * OOKLC_BYTES_PER_SYMBOL)

#define RSL_SYNCH_OFFSET        (RSL_STATE_OFFSET + RSL_STATE_SIZE)
#define RSL_SYNCH_SIZE          (4 * OOKLC_BYTES_PER_SYMBOL)

#define RSL_CMD_SIZE            (RSL_GROUP_ADDR_SIZE + RSL_ADDR_SIZE + RSL_RSVD_SIZE + RSL_STATE_SIZE + RSL_SYNCH_SIZE)


#define RSL_NUM_GROUPS      4
#define RSL_NUM_ADDRS       4
#define RSL_NUM_STATES      2


static uint8_t groupAddrs[RSL_NUM_GROUPS][RSL_GROUP_ADDR_SIZE] =
{
    {RSL_GROUP_ADDR_0}, {RSL_GROUP_ADDR_1}, {RSL_GROUP_ADDR_2}, {RSL_GROUP_ADDR_3}
};

static uint8_t switchAddrs[RSL_NUM_ADDRS][RSL_ADDR_SIZE] =
{
    {RSL_ADDR_0}, {RSL_ADDR_1}, {RSL_ADDR_2}, {RSL_ADDR_3}
};

static uint8_t switchState[RSL_NUM_STATES][RSL_STATE_SIZE] =
{
    {RSL_OFF}, {RSL_ON}
};

static uint8_t rsvd[RSL_RSVD_SIZE] =
{
    OOKLC_SYMBOL_F OOKLC_SYMBOL_F
};

static uint8_t sync[RSL_SYNCH_SIZE] =
{
    OOKLC_SYMBOL_S1 OOKLC_SYMBOL_S2 OOKLC_SYMBOL_S3 OOKLC_SYMBOL_S4
};


static uint8_t onOffCmdPkt[(RSL_CMD_SIZE) * RSL336_NUM_REPEAT_COMMANDS]; //must send multiple back to back commands

static uint8_t rawOnPacket[] = {
    RSL_GROUP_ADDR_0
    RSL_ADDR_0
    OOKLC_SYMBOL_F OOKLC_SYMBOL_F //unused address and data bits
    RSL_ON
    OOKLC_SYMBOL_S1
    //Send twice back to back
    RSL_GROUP_ADDR_0
    RSL_ADDR_0
    OOKLC_SYMBOL_F OOKLC_SYMBOL_F //unused address and data bits
    RSL_ON
    OOKLC_SYMBOL_S1
};

static uint8_t rawOffPacket[] = {
    RSL_GROUP_ADDR_0
    RSL_ADDR_0
    OOKLC_SYMBOL_F OOKLC_SYMBOL_F //unused address and data bits
    RSL_OFF
    OOKLC_SYMBOL_S1
    //Send twice back to back
    RSL_GROUP_ADDR_0
    RSL_ADDR_0
    OOKLC_SYMBOL_F OOKLC_SYMBOL_F //unused address and data bits
    RSL_OFF
    OOKLC_SYMBOL_S1
};

/***** Variable declarations *****/
static RSL336_Params OOKLCParams;

/***** Private function definitions *****/

/***** Public function definitions *****/

/** @brief  Function to initialize the RSL336_Params struct to its defaults
 *
 *  @param  params      An pointer to RF_Params structure for
 *                      initialization
 *
 */
void RSL336_Params_init(RSL336_Params *params)
{
}

/** @brief  Function to open the RSL366 module
 *
 *  @param  params      An pointer to RF_Params structure for initialization
 *
 *  @return RSL336_Stat status
 */
RSL336_Status RSL336_open(RSL336_Params *params)
{
    RSL336_Status status = RSL336_StatusError;
    OOKLC_Params ookParams;
    uint8_t cmdIdx;

    (void) params; //currently no params

    OOKLC_Params_init(&ookParams);
    
    ookParams.freq = RSL336_FREQUENCY;
    ookParams.dataRate = RSL336_BIT_RATE;

    // Initialize and open the OOK MAC/Phy
    OOKLC_init(&ookParams);
    OOKLC_open();

    for(cmdIdx = 0; cmdIdx < RSL336_NUM_REPEAT_COMMANDS; cmdIdx++)
    {
        //set rsvd fields
        memcpy(&onOffCmdPkt[RSL_RSVD_OFFSET + (RSL_CMD_SIZE*cmdIdx)], rsvd, RSL_RSVD_SIZE);

        //set synch
        memcpy(&onOffCmdPkt[RSL_SYNCH_OFFSET + (RSL_CMD_SIZE*cmdIdx)], sync, RSL_SYNCH_SIZE);
    }

/*
    //set rsvd fields
    memcpy(&onOffCmdPkt[RSL_RSVD_OFFSET],  rsvd, RSL_ADDR_SIZE);
    memcpy(&onOffCmdPkt[RSL_RSVD_OFFSET + RSL_CMD_SIZE], rsvd, RSL_RSVD_SIZE);

    //set synch
    memcpy(&onOffCmdPkt[RSL_SYNCH_OFFSET], sync, RSL_STATE_SIZE);
    memcpy(&onOffCmdPkt[RSL_SYNCH_OFFSET + RSL_CMD_SIZE], sync, RSL_STATE_SIZE);
*/
    return status;
}

/** @brief  Updates the policy used to make scheduling decisions
 *
 *  @param  groupAddr     Group address 0-3
 *  @param  switchAddr    Switch/Outlet address 0-3
 *  @param  state         On = 1, Off = 0
 *
 *  @return RSL336_Stat status
 */
RSL336_Status RSL336_SendOnCmd(uint8_t groupAddr, uint8_t switchAddr, bool state)
{
    OOKLC_Status ookStatus;
    uint8_t cmdIdx;

    if( (groupAddr > RSL_NUM_GROUPS) ||
        (switchAddr > RSL_NUM_ADDRS) )
    {
        //param error
        return RSL336_StatusParamError;
    }


    for(cmdIdx = 0; cmdIdx < RSL336_NUM_REPEAT_COMMANDS; cmdIdx++)
    {
        //set group addr
        memcpy(&onOffCmdPkt[RSL_GROUP_ADDR_OFFSET + (RSL_CMD_SIZE*cmdIdx)], groupAddrs[groupAddr], RSL_GROUP_ADDR_SIZE);
        //set switch addr
        memcpy(&onOffCmdPkt[RSL_ADDR_OFFSET + (RSL_CMD_SIZE*cmdIdx)], switchAddrs[switchAddr], RSL_ADDR_SIZE);
        //set state
        memcpy(&onOffCmdPkt[RSL_STATE_OFFSET + (RSL_CMD_SIZE*cmdIdx)], switchState[state], RSL_STATE_SIZE);
    }

/*
    //set group addr
    memcpy(&onOffCmdPkt[RSL_GROUP_ADDR_OFFSET], groupAddrs[groupAddr], RSL_GROUP_ADDR_SIZE);
    memcpy(&onOffCmdPkt[RSL_GROUP_ADDR_OFFSET + RSL_CMD_SIZE], groupAddrs[groupAddr], RSL_GROUP_ADDR_SIZE);
    //set switch addr
    memcpy(&onOffCmdPkt[RSL_ADDR_OFFSET], switchAddrs[switchAddr], RSL_ADDR_SIZE);
    memcpy(&onOffCmdPkt[RSL_ADDR_OFFSET + RSL_CMD_SIZE], switchAddrs[switchAddr], RSL_ADDR_SIZE);
    //set state
    memcpy(&onOffCmdPkt[RSL_STATE_OFFSET], switchState[state], RSL_STATE_SIZE);
    memcpy(&onOffCmdPkt[RSL_STATE_OFFSET + RSL_CMD_SIZE], switchState[state], RSL_STATE_SIZE);
*/

    static volatile bool sendRaw = 0;
    if(!sendRaw)
    {
        uint32_t len = sizeof(onOffCmdPkt);
        len = len / OOKLC_BYTES_PER_SYMBOL;

        ookStatus = OOKLC_txFrame((uint8_t*) &onOffCmdPkt, len);
    }
    else
    {
        /* Send packet 4 times */
        for(cmdIdx = 0; cmdIdx < 10; cmdIdx++)
        {
            if(state)
            {
                ookStatus = OOKLC_txFrame((uint8_t*) &rawOnPacket, sizeof(rawOnPacket) / OOKLC_BYTES_PER_SYMBOL);
            }
            else
            {
                ookStatus = OOKLC_txFrame((uint8_t*) &rawOffPacket, sizeof(rawOffPacket) / OOKLC_BYTES_PER_SYMBOL);
            }

        }
    }

    if(ookStatus == OOKLC_StatusSuccess)
    {
        return RSL336_StatusSuccess;
    }
    else
    {
        return RSL336_StatusTxFail;
    }
}
