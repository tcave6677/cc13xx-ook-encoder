/******************************************************************************

 @file rsl_336.h

 @brief API's for sending RSL336 encoded RF packets

 Group: WCS LPC
 $Target Device: DEVICES $

 ******************************************************************************
 $License: BSD3 2017 $
 ******************************************************************************
 $Release Name: PACKAGE NAME $
 $Release Date: PACKAGE RELEASE DATE $
 *****************************************************************************/
/*!****************************************************************************
 *
 *  The RSL336 interface provides a service to for the  application to
 *  encode OOK Symbol for controlling RSL336 outlets
 *
 *
 *  # Usage #
 *
 *
 ********************************************************************************/

#ifndef RSL_336_
#define RSL_336_

#include "stdint.h"
#include <ti/drivers/rf/RF.h>

/** @brief RSL336 parameter struct
 *  RSL336 parameters are used with the RSL336_open() and RSL336_Params_init() call.
 */
typedef struct {
} RSL336_Params;

/** @brief Status codes for various RSL336 functions.
 *
 *  RF_Stat is reported as return value for RSL336 functions.
 */
typedef enum {
    RSL336_StatusSuccess,         ///< Function finished with success
    RSL336_StatusParamError,      ///< Parameter Error
    RSL336_StatusTxFail,          ///< TX Failure
    RSL336_StatusError,           ///< Error
} RSL336_Status;

/** @brief  Function to initialize the RSL336_Params struct to its defaults
 *
 *  @param  params      An pointer to RF_Params structure for
 *                      initialization
 *
 *  Defaults values are:
 */
extern void RSL336_Params_init(RSL336_Params *params);

/** @brief  Function to init the RSL336 module
 *
 *  @param  params      An pointer to RF_Params structure for initialization
 *
 *  @return RSL336_Stat status
 */
extern RSL336_Status RSL336_init(RSL336_Params *params);

/** @brief  Updates the policy used to make scheduling decisions
 *
 *  @param  groupAddr     Group address 0-3
 *  @param  switchAddr    Switch/Outlet address 0-3
 *  @param  state         On = 1, Off = 0
 *
 *  @return RSL336_Stat status
 */
extern RSL336_Status RSL336_SendOnCmd(uint8_t groupAddr, uint8_t switchAddr, bool state);

#endif /* RSL_336_ */
