/******************************************************************************

 @file ook_line_coding.c

 @brief Dual Mode Manager Policy

 Group: WCS LPC
 $Target Device: DEVICES $

 ******************************************************************************
 $License: BSD3 2016 $
 ******************************************************************************
 $Release Name: PACKAGE NAME $
 $Release Date: PACKAGE RELEASE DATE $
 *****************************************************************************/

/***** Includes *****/
#include "ook_line_coding/ook_line_coding.h"
#include "smartrf_settings/smartrf_settings.h"

/***** Defines *****/

//! \brief macro to convert from ms to Radio Time Ticks
#define OOKLC_ms_To_RadioTime(ms) (ms*(4000000/1000))

/***** Variable declarations *****/
static OOKLC_Params OOKLCParams;
static RF_Object rfObject;
static RF_Handle rfHandle;

/***** Private function definitions *****/

OOKLC_Status setFrequency(uint32_t ui32Frequency)
{
    OOKLC_Status status = OOKLC_StatusSuccess;
    
    /* Set the frequency */
    RF_cmdFs.frequency = (uint16_t)(ui32Frequency / 1000000);
    RF_cmdFs.fractFreq = (uint16_t) (((uint64_t)ui32Frequency -
            ((uint64_t)RF_cmdFs.frequency * 1000000)) * 65536 / 1000000);

/* Todo set center freq
    RF_prop.centerFreq = 0x01B1,
    RF_prop.intFreq = 0x8000,
*/

    return status;
}

OOKLC_Status setDataRate(uint32_t ui32DataRate)
{
    OOKLC_Status status = OOKLC_StatusSuccess;
    uint32_t rateWord = (ui32DataRate * 24000000) / (RF_cmdPropRadioDivSetup.symbolRate.preScale * 100000);
    
    RF_cmdPropRadioDivSetup.symbolRate.rateWord = rateWord;

    return status;
}

/***** Public function definitions *****/

/** @brief  Function to initialize the OOKLC_Params struct to its defaults
 *
 *  @param  params      An pointer to RF_Params structure for
 *                      initialization
 *
 */
void OOKLC_Params_init(OOKLC_Params *params)
{
    params->freq = 43392000;
    params->dataRate = 2450;
}

/** @brief  Function to init the OOK Line Coding module
 *
 *  @param  params      An pointer to RF_Params structure for initialization
 *
 *  @return OOKLC_Stat status
 */
OOKLC_Status OOKLC_init(OOKLC_Params *params)
{
    OOKLC_Status status = OOKLC_StatusError;

    /* Populate default RF parameters if not provided */
    if (params != NULL)
    {
        memcpy(&OOKLCParams, params, sizeof(OOKLC_Params));
        status = OOKLC_StatusSuccess;
    }
    else
    {
        status = OOKLC_Status_ParamError;
    }

    /* Set frequency */
//    setFrequency(params->freq);

    /* Set data rate */
//    setDataRate(params->dataRate);

    return status;
}

/** @brief  Function to open the  OOK Line Coding module
 *
 *  @return OOKLC_Stat status
 */
OOKLC_Status OOKLC_open(void)
{
    OOKLC_Status status = OOKLC_StatusError;
    RF_Params rfParams;

    RF_Params_init(&rfParams);
    //set default InactivityTimeout to 1000us
    rfParams.nInactivityTimeout = 1000;
    
    /* Open radio driver handle*/
    rfHandle = RF_open(&rfObject, &RF_prop, (RF_RadioSetup*)&RF_cmdPropRadioDivSetup, &rfParams);

    /* Set the frequency */
    RF_postCmd(rfHandle, (RF_Op*)&RF_cmdFs, RF_PriorityNormal, NULL, 0);

    /* setup Tx packet */
    RF_cmdPropTx.startTrigger.triggerType = TRIG_NOW;
    RF_cmdPropTx.startTrigger.pastTrig = 1;
    RF_cmdPropTx.startTime = 0;

    
    return status;
}

/** @brief  Updates the policy used to make scheduling decisions
 *
 *  @param  pTxFrame      Frame of Symbols to Tx
 *  @param  txFrameLen    Number of Symbols in the frame
 *
 *  @return OOKLC_Stat status
 */
OOKLC_Status OOKLC_txFrame(uint8_t* pTxFrame, uint32_t txFrameLen)
{
    RF_EventMask rfStatus;
/*
    RF_cmdPropTx.pktLen = txFrameLen * OOKLC_BYTES_PER_SYMBOL;
    RF_cmdPropTx.pPkt = pTxFrame;
    
    rfStatus = RF_runCmd(rfHandle, (RF_Op*)&RF_cmdPropTx,
                           RF_PriorityNormal, NULL, 0);
*/

    RF_cmdPropTxAdv.pktLen = txFrameLen * OOKLC_BYTES_PER_SYMBOL;
    RF_cmdPropTxAdv.pPkt = pTxFrame;

    rfStatus = RF_runCmd(rfHandle, (RF_Op*)&RF_cmdPropTxAdv,
                           RF_PriorityNormal, NULL, 0);
    
    if(rfStatus == RF_EventLastCmdDone)
    {
        return OOKLC_StatusSuccess;
    }
    else
    {
        return OOKLC_StatusError;
    }
}
