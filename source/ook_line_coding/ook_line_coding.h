/******************************************************************************

 @file ook_line_coding.h

 @brief OOK Line Coding Header

 Group: WCS LPC
 $Target Device: DEVICES $

 ******************************************************************************
 $License: BSD3 2017 $
 ******************************************************************************
 $Release Name: PACKAGE NAME $
 $Release Date: PACKAGE RELEASE DATE $
 *****************************************************************************/
/*!****************************************************************************
 *  @file       ook_line_coding.h
 *
 *  @brief      Dual Mode Policy Manager
 *
 *  The OOK Line Coding interface provides a service to the application to
 *  format OOK Line coding Symbols and send them OTA. It can be thought of as
 *  an OOK MAC and Phy
 *
 *  # Usage #
 *
 *
 ********************************************************************************/

#ifndef OOKLC_H_
#define OOKLC_H_

#include "stdint.h"
#include <ti/drivers/rf/RF.h>

//! \brief stack timing
#define OOKLC_SYMBOL_0  0xf0, 0x00, 0xf0, 0x00,
#define OOKLC_SYMBOL_1  0xff, 0xf0, 0xff, 0xf0,
#define OOKLC_SYMBOL_F  0xf0, 0x00, 0xff, 0xf0,
#define OOKLC_SYMBOL_S1  0xf0, 0x00, 0x00, 0x00,
#define OOKLC_SYMBOL_S2  0x00, 0x00, 0x00, 0x00,
#define OOKLC_SYMBOL_S3  0x00, 0x00, 0x00, 0x00,
#define OOKLC_SYMBOL_S4  0x00, 0x00, 0x00, 0x00,


#define OOKLC_BYTES_PER_SYMBOL 4

/** @brief OOK Line Coding parameter struct
 *  OOK Line Coding parameters are used with the OOKLC_open() and OOKLC_Params_init() call.
 */
typedef struct {
    uint32_t freq;     //!< frequency in Hz
    uint32_t dataRate; //!< dataRate in bps
} OOKLC_Params;

/** @brief Status codes for various OOK Line Coding functions.
 *
 *  RF_Stat is reported as return value for OOK Line Coding functions.
 */
typedef enum {
    OOKLC_StatusSuccess,        ///< Function finished with success
    OOKLC_Status_ParamError,    ///< OOK Parameter error
    OOKLC_StatusError,          ///< Error
} OOKLC_Status;

/** @brief  Function to initialize the OOKLC_Params struct to its defaults
 *
 *  @param  params      An pointer to RF_Params structure for
 *                      initialization
 *
 *  Defaults values are:
 */
extern void OOKLC_Params_init(OOKLC_Params *params);

/** @brief  Function to init the RSL336 module
 *
 *  @param  params      An pointer to RF_Params structure for initialization
 *
 *  @return OOKLC_Stat status
 */
extern OOKLC_Status OOKLC_init(OOKLC_Params *params);

/** @brief  Function to open the RSL336 module
 *
 *  @return OOKLC_Stat status
 */
extern OOKLC_Status OOKLC_open(void);


/** @brief  Updates the policy used to make scheduling decisions
 *
 *  @param  pTxFrame      Frame of Symbols to Tx
 *  @param  txFrameLen    Number of Symbols in the frame
 *
 *  @return OOKLC_Stat status
 */
extern OOKLC_Status OOKLC_txFrame(uint8_t* pTxFrame, uint32_t txFrameLen);

#endif /* OOKLC_H_ */
