This repository contains projects that allow the Texas Instruments C13xx family of devices to control 433Mz OOK (On OFF Keying) devices.

Currently there is support for the following platforms:

- CC1310 LuanchPad ([available here](http://store.ti.com/LAUNCHXL-CC1310-SimpleLink-Sub-1-GHz-wireless-microcontroller-MCU-LaunchPad-development-kit-P49907.aspx))
- CC1350 Sensor Tag ([available here](https://store.ti.com/CC1350STKEU-Simplelink-CC1350-SensorTag-Bluetooth-and-Sub-1GHz-Long-Range-Wireless-Development-Kit-P50808.aspx)). The [debug devpack](https://store.ti.com/cc-devpack-debug.aspx) is also needed to download and debug.

Currently only the RLS366 outlets are supported.

To use these examples:

1. Install the Code Composer Studio IDE from [here](http://processors.wiki.ti.com/index.php/Download_CCS).
2. Install the CC13x0 SDK from [here](http://www.ti.com/tool/SIMPLELINK-CC13X0-SDK). Install to the default dir to avoid needing to change the preferences in CCS
2. Clone this repository:

	```git clone https://tcave6677@bitbucket.org/tcave6677/cc13xx-ook-encoder.git```
 
3. Open CCS and import the project from the cloned repo

	```<repo base dir>\example\<platform>\rsl366_controller\tirtos\ccs```

    Where the <platofrm> dir should match the Luanch pad or sensortag (STK) that you want to build for.
    
4. Build, download and run.

5. The left button will toggle outlet 1 of group 1.
